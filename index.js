var fs        = require('fs')
var csv       = 'rbnb.csv'
var Nightmare = require('nightmare')
var writer    = require('json2csv')
var reader    = require('csvtojson')
var async     = require('async')
var nm_options= {
  show: false, 
  webPreferences: {
    images: false, 
    partition: 'nopersist'
  }
  // ,switches: {'proxy-server': 'iad-a11.ipvanish.com:80'}
}
var quote_span= 'span[class^="estimateWrapper"] > span'

readCsv().then((data) => {
  main (data, 0)
})

function main (data, index) {
  console.log(new Date().toUTCString(), 'line ' + parseInt(index + 1))
  if (!data[index]) console.log('there is no more line to proceed')
  else if (
    data[index]['Accommodates 2'].length > 0
    && data[index]['Accommodates 4'].length > 0
    && data[index]['Accommodates 6'].length > 0
    && data[index]['Accommodates 8'].length > 0
  ) main (data, index + 1)
  else {
    return getQuotes (data[index]['City State'])
    .then((quotes) => {
      data[index]['Accommodates 2'] = quotes[0]
      data[index]['Accommodates 4'] = quotes[1]
      data[index]['Accommodates 6'] = quotes[2]
      data[index]['Accommodates 8'] = quotes[3]
      console.log(data[index])
      writeCsv (data)
      // setTimeout(() => { main (data, index + 1) }, 25000) // 
      main (data, index + 1)
    })
  }
}

function getQuotes (city) {
  return new Promise ((cb) => {
    var nightmare = Nightmare(nm_options)
    nightmare
      .goto('https://www.airbnb.com/co-hosting?ref=nav_dropdown')
      .select('#capacity', 15)// preventive
      .type('#address', city)
      .evaluate (() => {
        $('#address').focus()
      })
      .wait('ul#address__listbox li:first-child')
      .click('ul#address__listbox li:first-child')
      .wait(quote_span)
      .evaluate((quote_span) => {
        return $(quote_span).html()
      }, quote_span)
      .then((initval) => {
        console.log('initval', initval)
        var browserparam = {
          quote_span: quote_span,
          initval: initval
        }
        var quotes = []
        return switchCapacity(nightmare, 2, browserparam)
        .then((q) => {
          quotes.push(q)
          return switchCapacity(nightmare, 4, browserparam)
        })
        .then((q) => {
          quotes.push(q)
          return switchCapacity(nightmare, 6, browserparam)
        })
        .then((q) => {
          quotes.push(q)
          return switchCapacity(nightmare, 8, browserparam)
        })
        .then((q) => {
          quotes.push(q)
          cb (quotes)
        })
      })
      .catch((e) => {
        console.error(e)
        cb (['', '', '', ''])
      })
  })
}

function switchCapacity (nm, capacity, browserparam) {
  return new Promise ((cb) => {
    nm
    .wait(quote_span)
    .evaluate((browserparam) => {
      document.querySelector(browserparam.quote_span).innerHTML = browserparam.initval
    }, browserparam)
    .select('#capacity', capacity)
    .wait(quote_span)
    .wait((browserparam) => {
      return document.querySelector(browserparam.quote_span).innerHTML != browserparam.initval
    }, browserparam)
    .evaluate((browserparam) => {
      return $(browserparam.quote_span).html()
    }, browserparam)
    if (8 == capacity) nm.end()
    nm.then((q) => {
      cb (q)
    })
  })
}

function readCsv () {
  return new Promise ((res, rej) => {
    var rows = []
    reader()
      .fromFile(csv)
      .on('json', (row, index) => {
        rows.push(row)
      })
      .on('done', () => {
        res (rows)
      })    
  })
}

function writeCsv (data) {
  return new Promise ((res, rej) => {
    result = writer({ data: data, fields: Object.keys(data[0])})
    fs.writeFile(csv, result, () => {
      res (true)
    })
  })
}