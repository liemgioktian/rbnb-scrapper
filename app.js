var fs        = require('fs')
var csv       = 'rbnb.csv'
var Nightmare = require('nightmare')
var writer    = require('json2csv')
var reader    = require('csvtojson')
var async     = require('async')
var nm_options= {
  waitTimeout: 15000,
  typeInterval: 50,
  show: true, 
  webPreferences: {
    images: false, 
    partition: 'nopersist'
  }
}
var quote_span= 'span[class^="estimateWrapper"] > span'

main ()
function main () {
  var nightmare = Nightmare(nm_options)
  return readCsv()
  .then((data) => {
    nightmare
    .goto('https://www.airbnb.com/co-hosting?ref=nav_dropdown')
    return doLoop (nightmare, data, 0, 2)
  })
}

function doLoop (nm, data, index, capacity) {
  return new Promise (() => {
    while (
      typeof (data[index]) !== 'undefined'
      &&
      (
        data[index]['Accommodates ' + capacity].length > 0 
        && 
        data[index]['Accommodates ' + capacity].indexOf('Rp') == -1
      )
    ) {
      switch (capacity) {
        case 2: capacity = 4 ;break
        case 4: capacity = 6 ;break
        case 6: capacity = 8 ;break
        case 8: 
          capacity= 2 
          index   = parseInt(index) + 1
          ;break
      }
    }

    if (typeof (data[index]) === 'undefined') nm.end().then()
    else nm
    .evaluate((quote_span) => {
      $(quote_span).html('')
    }, quote_span)
    .select('#capacity', capacity)
    .evaluate(() => {
      $('#address').focus()
      $('#address').attr('value', '')
    })
    .wait(() => {
      return $('#address').attr('value') == ''
    })
    .type('#address', data[index]['City State'])
    .evaluate (() => {
      $('#address').focus()
    })
    .wait('ul#address__listbox li:first-child')
    .click('ul#address__listbox li:first-child')
    .wait(quote_span)
    .wait((quote_span) => {
      return document.querySelector(quote_span).innerHTML != ''
    }, quote_span)
    .evaluate((quote_span) => {
      return $(quote_span).html()
    }, quote_span)
    .then((quote) => {
      data[index]['Accommodates ' + capacity] = quote
      console.log(data[index])
      writeCsv (data)
      return nextLoop (nm, data, index, capacity)
    })
    .catch((e) => {
      console.log(data[index]['City State'], capacity, e)
      return nextLoop (nm, data, index, capacity)
    })
  })

}

function nextLoop (nm, data, index, capacity) {
  return new Promise (() => {
    switch (capacity) {
      case 2: capacity = 4 ;break
      case 4: capacity = 6 ;break
      case 6: capacity = 8 ;break
      case 8: 
        capacity= 2 
        index   = parseInt(index) + 1
        ;break
    }
    if (!data[index]) nm.end().then()
    else if (capacity == 2 && index % 100 == 0) {
      nm.end().then()
      var nmx = Nightmare(nm_options)
      nmx.goto('https://www.airbnb.com/co-hosting?ref=nav_dropdown')
      return doLoop (nmx, data, index, capacity)
    } else return doLoop (nm, data, index, capacity)
  })
}

function readCsv () {
  return new Promise ((res, rej) => {
    var rows = []
    reader()
      .fromFile(csv)
      .on('json', (row, index) => {
        rows.push(row)
      })
      .on('done', () => {
        res (rows)
      })    
  })
}

function writeCsv (data) {
  return new Promise ((res, rej) => {
    result = writer({ data: data, fields: Object.keys(data[0])})
    fs.writeFile(csv, result, () => {
      res (true)
    })
  })
}